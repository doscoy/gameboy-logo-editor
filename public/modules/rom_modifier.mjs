"use strict";

import {LogoEditor} from "./logo_editor.mjs"

export class GBRomModifier {
  static #logo_address = 0x0104;
  static #logo_length = 48;
  static #global_checksum_address = 0x014E;
  static #global_checksum_length = 2;
  #target;
  #rom_load = document.createElement("input");
  #rom_save = document.createElement("button");
  #rom = null;
  #editor;
  #hex_text = document.createElement("textarea");
  #hex_input = document.createElement("button");
  #hex_output= document.createElement("button");

  constructor(div, init_logo = null) {
    this.#target = div;

    this.#rom_load.type = "file";
    this.#rom_load.accept = ".gb";
    this.#rom_load.addEventListener("input", () => this.#InputFileCallback());
    this.#target.appendChild(this.#rom_load);

    this.#rom_save.innerText = "save";
    this.#rom_save.style.display = "none";
    this.#rom_save.onclick = () => this.#SaveRomCallback();
    this.#target.appendChild(this.#rom_save);
    this.#target.appendChild(document.createElement("br"));

    this.#editor = new LogoEditor(this.#target, 15);
    if (init_logo != null) this.#editor.SetData(init_logo);

    this.#hex_text.style.marginTop = 8 + "px";
    this.#hex_text.style.width = this.#editor.GetWidth() + "px";
    this.#hex_text.style.height = 100 + "px";
    this.#hex_text.style.resize = "none";
    this.#target.appendChild(this.#hex_text);
    this.#target.appendChild(document.createElement("br"));

    this.#hex_input.innerText = "input";
    this.#hex_input.onclick = () => this.#HexText2Logo();
    this.#target.appendChild(this.#hex_input);

    this.#hex_output.innerText = "output";
    this.#hex_output.onclick = () => this.#Logo2HexText();
    this.#target.appendChild(this.#hex_output);
  }

  #InputFileCallback() {
    if (this.#rom_load.files.length == 0) {
      this.#rom_save.style.display = "none";
      return;
    }
    let reader = new FileReader();
    reader.onload = (event) => this.#LoadRomCallback(event);
    reader.readAsArrayBuffer(this.#rom_load.files[0]);
  }

  #LoadRomCallback(event) {
    this.#rom = event.target.result;
    this.#rom_save.style.display = "";
    this.#SetRomData2Editor();
  }

  #SetRomData2Editor() {
    const start = GBRomModifier.#logo_address;
    const end = GBRomModifier.#logo_address + GBRomModifier.#logo_length;
    const data = new Uint8Array(this.#rom).slice(start, end);
    this.#editor.SetData(data);
  }

  #SaveRomCallback() {
    this.#SetEditorData2Rom();
    const blob = new Blob([this.#rom], { type : "application/octet-stream" });
    const dummy = document.createElement("a");
    dummy.href = URL.createObjectURL(blob);
    dummy.download = this.#rom_load.files[0].name;
    dummy.click();
    URL.revokeObjectURL(dummy.href);
  }

  #SetEditorData2Rom() {
    const data = this.#editor.GetData();
    const rom = new Uint8Array(this.#rom);
    for (let i = 0; i < GBRomModifier.#logo_length; ++i) {
      rom[GBRomModifier.#logo_address + i] = data[i];
    }
    this.#SetGlobalChecksum2Rom();
  }

  #SetGlobalChecksum2Rom() {
    const rom = new Uint8Array(this.#rom);
    const head1 = 0;
    const tail1 = GBRomModifier.#global_checksum_address;
    const head2 = tail1 + GBRomModifier.#global_checksum_length;
    const tail2 = rom.length;
    let accum = 0;
    for (let i = head1; i < tail1; ++i) { accum += rom[i]; }
    for (let i = head2; i < tail2; ++i) { accum += rom[i]; }
    rom[GBRomModifier.#global_checksum_address + 0] = (accum >>> 8) & 0xFF;
    rom[GBRomModifier.#global_checksum_address + 1] = accum & 0xFF;
  }

  #HexText2Logo() {
    const delete_return = this.#hex_text.value.replace(/\n/g, " ");
    const data = delete_return.split(" ").map(item => parseInt(item, 16));
    this.#editor.SetData(data);
  }

  #Logo2HexText() {
    let counter = 0;
    const str = this.#editor.GetData().reduce((acum, item) => {
      const hex = item.toString(16).toUpperCase();
      const separator = counter == 15 ? "\n" : " ";
      counter = (counter + 1) % 16;
      return acum + (("00" + hex).substr(-2) + separator);
    }, "");
    this.#hex_text.value = str;
  }
};