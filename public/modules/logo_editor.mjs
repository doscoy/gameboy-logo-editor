"use strict";

import {LogoData} from "./logo_data.mjs"
import {LayerCanvas} from "./layer_canvas.mjs"

export class LogoEditor {
  #data = new LogoData();
  #target;
  #scale;
  #canvases;
  #contexts;
  #draw_buffer = 0;
  #coord_buffer = {x : -1, y : -1};

  constructor(div, scale = 10) {
    this.#target = div;
    this.#scale = scale;
    this.#canvases = new LayerCanvas(this.#target, LogoData.width * this.#scale, LogoData.height * this.#scale, 4);
    this.#contexts = this.#canvases.GetContexts("2d");
    this.#DrawGrid(2);
    this.#canvases.addEventListener("mousemove", event => this.#MouseMoveCallback(event));
    this.#canvases.addEventListener("mousedown", event => this.#MouseDownCallback(event));
    this.#canvases.addEventListener("mouseleave", event => this.#MouseLeaveCallback(event));
  }

  #GetMouseCoord(event) {
    const rect = event.target.getBoundingClientRect();
    return {
      x: Math.floor((event.x - rect.left) / this.#scale),
      y: Math.floor((event.y - rect.top) / this.#scale)
    };
  }

  #MouseMoveCallback(event) {
    const coord = this.#GetMouseCoord(event);
    if (coord.x == this.#coord_buffer.x && coord.y == this.#coord_buffer.y) return;
    this.#coord_buffer.x = coord.x;
    this.#coord_buffer.y = coord.y;
    this.#ClearLayer(3);
    this.#DrawForcus(3, coord);
    if (event.buttons == 0) return;
    this.#data.PutPixel(coord.x, coord.y, this.#draw_buffer);
    this.#DrawPixel(0, coord.x, coord.y);
  }

  #MouseDownCallback(event) {
    const coord = this.#GetMouseCoord(event);
    this.#draw_buffer = this.#data.FlipPixel(coord.x, coord.y);
    this.#DrawPixel(0, coord.x, coord.y);
  }

  #MouseLeaveCallback(event) {
    this.#coord_buffer.x = -1;
    this.#coord_buffer.y = -1;
    this.#ClearLayer(3);
  }

  #ClearLayer(layer) {
    this.#contexts[layer].clearRect(0, 0, LogoData.width * this.#scale, LogoData.height * this.#scale);
  }

  #DrawPixel(layer, x, y) {
    if (this.#data.PickupPixel(x, y)) {
      this.#contexts[layer].strokeStyle = "black";
      this.#contexts[layer].fillRect(x * this.#scale, y * this.#scale, this.#scale, this.#scale);
    } else {
      this.#contexts[layer].clearRect(x * this.#scale, y * this.#scale, this.#scale, this.#scale);
    }
  }

  #DrawAllPixel(layer) {
    for (let y = 0; y < LogoData.height; ++y) {
      for (let x = 0; x < LogoData.width; ++x) {
        const pixel = this.#data.PickupPixel(x, y);
        if (this.#data.PickupPixel(x, y)) {
          this.#contexts[layer].strokeStyle = "black";
          this.#contexts[layer].fillRect(x * this.#scale, y * this.#scale, this.#scale, this.#scale);
        } else {
          this.#contexts[layer].clearRect(x * this.#scale, y * this.#scale, this.#scale, this.#scale);
        }
      }
    }
  }

  #DrawGrid(layer) {
    for (let row = 0; row < LogoData.height; ++row) {
      for (let col = 0; col < LogoData.width; ++col) {
        this.#contexts[layer].lineWidth = 0.1;
        this.#contexts[layer].strokeStyle = "black";
        this.#contexts[layer].strokeRect(col * this.#scale, row * this.#scale, this.#scale, this.#scale);
      }
    }
    const tile_width = LogoData.width / 4;
    const tile_height = LogoData.height / 4;
    const tile_size = this.#scale * 4;
    for (let row = 0; row < tile_height; ++row) {
      for (let col = 0; col < tile_width; ++col) {
        this.#contexts[layer].lineWidth = 0.5;
        this.#contexts[layer].strokeStyle = "black";
        this.#contexts[layer].strokeRect(col * tile_size, row * tile_size, tile_size, tile_size);
      }
    }
  }

  #DrawForcus(layer, coord) {
    this.#contexts[layer].lineWidth = 1.0;
    this.#contexts[layer].strokeStyle = "red";
    this.#contexts[layer].strokeRect(coord.x * this.#scale, coord.y * this.#scale, this.#scale, this.#scale);
  }

  GetData() { return this.#data.GetData(); }

  SetData(data) {
    this.#data.SetData(data);
    this.#DrawAllPixel(0);
  }

  GetWidth() { return LogoData.width * this.#scale; }
};