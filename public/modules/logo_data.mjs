"use strict";

export class LogoData {
  static width = 48;
  static height = 8;
  static tile_size = 4;
  #data = new Uint8Array(48);

  SetData(data) {
    const len = Math.min(data.length, this.#data.length);
    for (let i = 0; i < len; ++i) {
      this.#data[i] = data[i];
    }
  }

  GetData() { return this.#data.map(item => item); }

  GetTileData() {
    const tile = new Uint8Array(this.#data.length / 2 * 16);
    this.#data.forEach((byte, index) => {
      let buffer = 0;
      for (let bit = 7; bit >= 0; --bit) {
        const flag = (byte & (1 << bit)) != 0 ? 1 : 0;
        buffer = (buffer << 1) | flag;
        buffer = (buffer << 1) | flag;
      }
      const first = (buffer >>> 8) & 0xFF;
      const second = buffer & 0xFF;
      const tile_head = index * 8;
      tile[tile_head + 0] = first;
      tile[tile_head + 1] = 0;
      tile[tile_head + 2] = first;
      tile[tile_head + 3] = 0;
      tile[tile_head + 4] = second;
      tile[tile_head + 5] = 0;
      tile[tile_head + 6] = second;
      tile[tile_head + 7] = 0;
    });
    return tile;
  }

  static #GetDataPos(x, y) {
    x = x % LogoData.width;
    y = y % LogoData.height;
    const tile = {
      x : Math.floor(x / LogoData.tile_size),
      y : Math.floor(y / LogoData.tile_size)
    };
    const sub_tile = {
      x : x - (tile.x * LogoData.tile_size),
      y : y - (tile.y * LogoData.tile_size)
    };
    const tile_index = tile.x + (tile.y * (LogoData.width / LogoData.tile_size));
    const data_index = (tile_index * 2) + Math.floor(sub_tile.y / 2);
    const bit = sub_tile.x + ((sub_tile.y % 2) * LogoData.tile_size);
    return {
      index : data_index,
      bit : 7 - bit
    }
  }

  FlipPixel(x, y) {
    const pos = LogoData.#GetDataPos(x, y);
    this.#data[pos.index] ^= (1 << pos.bit);
    return (this.#data[pos.index] >> pos.bit) & 1;
  }

  PutPixel(x, y, pixel) {
    const pos = LogoData.#GetDataPos(x, y);
    if (pixel) {
      this.#data[pos.index] |= (1 << pos.bit);
    } else {
      this.#data[pos.index] &= ~(1 << pos.bit);
    }
  }

  PickupPixel(x, y) {
    const pos = LogoData.#GetDataPos(x, y);
    return (this.#data[pos.index] >> pos.bit) & 1;
  }
};